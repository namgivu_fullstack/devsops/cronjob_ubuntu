if [ -z "$BASH" ]; then  bash $0; exit; fi  # forced run w/ bash shell ref. https://stackoverflow.com/a/15731952/248616
SH=`dirname $(realpath $BASH_SOURCE)`
AH=`realpath "$SH/.."`

cd $AH
    PYTHONPATH=$AH \
    python3 -m pipenv run \
        python "$AH/app.py"
