SH=`dirname $(realpath $BASH_SOURCE)`
DH=`realpath "$SH/../.."`

p='cronjob_runner__00_mockapi'
CRON_TIME='*/2 * * * *'

   BUILD_CONTEXT="$SH/buildcontext_d"
BUILD_DOCKERFILE="$DH/Dockerfile"
         LOG_DIR="$BUILD_CONTEXT/log"

# validate input envvar
[[ ! -d $BUILD_CONTEXT ]] && "Folder not exists at $BUILD_CONTEXT"
