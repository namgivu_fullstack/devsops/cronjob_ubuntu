SH=`dirname $(realpath $BASH_SOURCE)`
DH=`realpath "$SH/../.."`

set -a; source "$SH/config.sh"

docker-compose -p$p \
    -f "$DH/docker-compose.yml" down
