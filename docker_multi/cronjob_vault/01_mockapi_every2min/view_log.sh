SH=`dirname $(realpath $BASH_SOURCE)`
DH=`realpath "$SH/../.."`

cd $SH
    set -a; source "$SH/config.sh"

    # chown so as logfile not to be root
    sudo chown -R$USER:$USER $LOG_DIR

    # view log
    docker-compose -p$p -f "$DH/docker-compose.yml"   logs
    docker-compose -p$p -f "$DH/docker-compose.yml"   exec c cat /app/log/log.txt
