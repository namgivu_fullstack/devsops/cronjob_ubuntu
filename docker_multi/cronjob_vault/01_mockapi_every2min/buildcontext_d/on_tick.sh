if [ -z "$BASH" ]; then  bash $0; exit; fi  # forced run w/ bash shell ref. https://stackoverflow.com/a/15731952/248616
SH=`dirname $(realpath $BASH_SOURCE)`
AH=`realpath "$SH/python_app"`

run="$AH/script/run.sh"
echo --- `date "+%Y-%m-%d %H:%M:%S"` $run ...
$run
echo --- `date "+%Y-%m-%d %H:%M:%S"` $run ... Done
