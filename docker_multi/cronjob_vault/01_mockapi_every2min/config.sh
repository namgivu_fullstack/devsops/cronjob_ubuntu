SH=`dirname $(realpath $BASH_SOURCE)`
DH=`realpath "$SH/../.."`

p='01_mockapi_every2min'
CRON_TIME='*/2 * * * *'  # every day at 2AM ref. https://crontab.guru/every-day-at-2am

   BUILD_CONTEXT="$SH/buildcontext_d"
BUILD_DOCKERFILE="$DH/Dockerfile"
         LOG_DIR="$BUILD_CONTEXT/log"

# validate input envvar
[[ ! -d $BUILD_CONTEXT ]] && "Folder not exists at $BUILD_CONTEXT"
