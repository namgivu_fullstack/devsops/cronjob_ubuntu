SH=`dirname $(realpath $BASH_SOURCE)`

set -a; source "$SH/config.sh"

if [ ! -z $nocache ]; then  # force rebuild docker image with NO CACHE ref. https://stackoverflow.com/a/42728416/248616
    docker-compose -p$p \
        -f "$SH/docker-compose.yml" build \
        --no-cache
fi

docker-compose -p$p \
    -f "$SH/docker-compose.yml" up \
    -d --build  --force-recreate
    #           --force-recreate forced recreate container
    #  --build build images before starting containers

echo; docker-compose -p$p \
    -f "$DH/docker-compose.yml" \
    ps
