if [ -z "$BASH" ]; then  bash $0; exit; fi  # forced run w/ bash shell ref. https://stackoverflow.com/a/15731952/248616
SH=`dirname $(realpath $BASH_SOURCE)`
AH=`realpath "$SH/python_app"`

cd "$AH"
    python3 -m pipenv --rm  # rm any existing .venv

    PIPENV_VENV_IN_PROJECT=1 \
        python3 -m pipenv install

    python3 -m pipenv --venv  # see .venv path
