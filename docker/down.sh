SH=`dirname $(realpath $BASH_SOURCE)`

set -a; source "$SH/config.sh"

docker-compose -p$p \
    -f "$SH/docker-compose.yml" down
